import re
import numpy as np
from keras.models import load_model, Sequential
from keras.layers import Dense, Dropout, Activation, Embedding, LSTM, GRU, Bidirectional
from keras.callbacks import ModelCheckpoint
from preprocess import Preprocessing

class eosTrain(object):

    def __init__(self):
        self.preproc = Preprocessing()

    def train(self, trainingFile,
            windowNgram,
            epochs,
            batchSize,
            dropout,
            maxFeatures,
            embeddingSize,
            lstmGruSize,
            modelFilename,
            indexFilename):
        """ Trains the BiLSTM model and saves the model and the char Index (vocab).
        # Arguments:
          trainingFile: the file to train on
          windowNgram: the context window size to consider around a potential eos (in terms of a char number)
          epochs: Number of training epochs
          dropout: Dropout rate
          maxFeatures: Number of features for Embeddings layer
          embeddingSize: Embeddings size
          lstmGruSize: Size of LSTM/GRU layer
          modelFilename: the trained model file
          indexFilename: the char index file
          windowNgram: the context window size to consider around a potential eos (in terms of a char number)
          batchSize: Size of batch of the model
        """

        # Reading training corpus
        with open(trainingFile, mode='r', encoding='utf-8') as f:
            trainingCorpus = f.read()

        # Building dataSet from training corpus
        ngrams = self.preproc.buildNgramsfromTrainingCorpus(trainingCorpus, windowNgram)
        charIndex = self.preproc.buildCharIndex(ngrams)
        dataSet = self.preproc.buildDataset(ngrams, charIndex, windowNgram)

        # Extraction of training features
        x_train = np.array([i[1] for i in dataSet])
        y_train = np.array([i[0] for i in dataSet])

        # Building BiLSTM model
        model = Sequential()
        model.add(Embedding(maxFeatures,embeddingSize))
        model.add(Bidirectional(LSTM(lstmGruSize,dropout=dropout,recurrent_dropout=dropout)))
        model.add(Dense(1, activation='sigmoid'))
        model.compile(loss='binary_crossentropy',optimizer='adam',metrics=['accuracy'])
        model.summary()
        mcp = ModelCheckpoint(modelFilename,monitor="val_accuracy",save_best_only=True, save_weights_only=False,mode='max')
        model.fit(x_train, y_train, validation_split=0.33, epochs=epochs, batch_size=batchSize,callbacks=[mcp])

        # Saving the char Index to use it for test
        self.preproc.saveCharIndex(charIndex, indexFilename)
