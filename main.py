import argparse
import confuse
import logging

from train import eosTrain
from test import eosTest
from preprocess import Preprocessing

# Runninig the training mode
def train(config,lang):

    try:
        trainingFile = config['data'][lang]['trainingFile'].get()
        modelFilename = config['data'][lang]['modelFilename'].get()
        indexFilename = config['data'][lang]['indexFilename'].get()
        epochs = config['bilstm']['epochs'].get()
        batchSize = config['bilstm']['batchSize'].get()
        dropout = config['bilstm']['dropout'].get()
        maxFeatures = config['bilstm']['maxFeatures'].get()
        embeddingSize = config['bilstm']['embeddingSize'].get()
        lstmGruSize = config['bilstm']['lstmGruSize'].get()
        windowNgram = config['preprocessing']['windowNgram'].get()
    except (confuse.ConfigReadError) as e:
        logging.critical("Config read error")

    eos = eosTrain()
    eos.train(trainingFile,
              int(windowNgram),
              int(epochs),
              int(batchSize),
              float(dropout),
              int(maxFeatures),
              int(embeddingSize),
              int(lstmGruSize),
              modelFilename,
              indexFilename)

# Runninig the predicting mode
def predict(config,lang,testFile,testOutputFile,html):

    try:
        modelFilename = config['data'][lang]['modelFilename'].get()
        indexFilename = config['data'][lang]['indexFilename'].get()
        batchSize = config['bilstm']['batchSize'].get()
        windowNgram = config['preprocessing']['windowNgram'].get()
    except (confuse.ConfigReadError) as e:
        logging.critical("Config read error")

    eos = eosTest()
    eos.predict(testFile,
                modelFilename,
                indexFilename,
                testOutputFile,
                html,
                int(windowNgram),
                int(batchSize))


# Runninig the evaluating mode
def evaluate(testOutputFile, testGoldFile):

    peos = eosTest()
    peos.evaluate(testOutputFile, testGoldFile)


# Runninig the predicting and evaluating mode
def predictAndEvaluate(config, lang, testFile, testOutputFile, testGoldFile, html):

    try:
        modelFilename = config['data'][lang]['modelFilename'].get()
        indexFilename = config['data'][lang]['indexFilename'].get()
        batchSize = config['bilstm']['batchSize'].get()
        windowNgram = config['preprocessing']['windowNgram'].get()
    except (confuse.ConfigReadError) as e:
        logging.critical("Config read error")

    # Test
    peos = eosTest()
    peos.predict(testFile,
                    modelFilename,
                    indexFilename,
                    testOutputFile,
                    html,
                    int(windowNgram),
                    int(batchSize))
    # Evaluate
    peos.evaluate(testOutputFile, testGoldFile)

# Runninig the complete pipeline mode
def sbdPipeline(config, lang):

    try:
        trainingFile = config['data'][lang]['trainingFile'].get()
        modelFilename = config['data'][lang]['modelFilename'].get()
        indexFilename = config['data'][lang]['indexFilename'].get()
        epochs = config['bilstm']['epochs'].get()
        batchSize = config['bilstm']['batchSize'].get()
        dropout = config['bilstm']['dropout'].get()
        maxFeatures = config['bilstm']['maxFeatures'].get()
        embeddingSize = config['bilstm']['embeddingSize'].get()
        lstmGruSize = config['bilstm']['lstmGruSize'].get()
        windowNgram = config['preprocessing']['windowNgram'].get()
        trainRatioSize = config['preprocessing']['trainRatioSize'].get()
    except (confuse.ConfigReadError) as e:
        logging.critical("Config read error")

    # Splits data into train and test and builds the gold standard
    preprocessing = Preprocessing()
    preprocessing.prepareData(trainingFile, trainRatioSize)

    # Train
    eos = eosTrain()
    eos.train(trainingFile + ".train",
                int(windowNgram),
                int(epochs),
                int(batchSize),
                float(dropout),
                int(maxFeatures),
                int(embeddingSize),
                int(lstmGruSize),
                modelFilename,
                indexFilename)

    # Test
    peos = eosTest()
    peos.predict(trainingFile + ".test",
                    modelFilename,
                    indexFilename,
                    trainingFile + ".test.output",
                    False,
                    int(windowNgram),
                    int(batchSize))
    # Evaluate
    peos.evaluate(trainingFile + ".test.output", trainingFile + ".test.gold")



# Parsing the arguments
def parseArgs():

    # Defining the parser
    parser = argparse.ArgumentParser()

    # Arguments from the command Line
    parser.add_argument("mode", choices=["train","predict","predictAndEvaluate","evaluate","sbdPipeline"])
    parser.add_argument("lang", choices=["FR","EN"])
    parser.add_argument("--html", dest='html', action='store_true')
    parser.set_defaults(html=False)
    parser.add_argument("--testFile", help="Defines test data")
    parser.add_argument("--testOutputFile", help="Defines the output data (predicted sentences)")
    parser.add_argument("--testGoldFile", help="Defines test gold standard data")
    args = parser.parse_args()

    # Config file
    config = confuse.Configuration('SentenceBoundariesDetection')
    logging.basicConfig(format='%(levelname)s:%(message)s',level=logging.DEBUG)
    try:
        config.set_file("config.yaml")
    except (confuse.NotFoundError) as e:
        logging.critical("config.yaml file not found")

    config.set_args(args)

    # Training mode
    if args.mode == "train":
        train(config, args.lang)

    # Test mode
    elif args.mode == "predict":
        if not args.testFile:
            print("***************************************************************************************")
            print("Usage main.py predict lang [--html] --testFile TESTFILE --testOutputFile TESTOUTPUTFILE")
            print("***************************************************************************************")
            print("Test file path is missing!")
            exit(1)
        if not args.testOutputFile:
            print("***************************************************************************************")
            print("Usage main.py predict lang [--html] --testFile TESTFILE --testOutputFile TESTOUTPUTFILE")
            print("***************************************************************************************")
            print("Output file path is missing!")
            exit(1)

        predict(config, args.lang, args.testFile, args.testOutputFile, args.html)

    # Test and evaluate
    elif args.mode == "predictAndEvaluate":

        if not args.testFile:
            print("******************************************************************************************************************************")
            print("Usage main.py predictAndEvaluate lang [--html] --testFile TESTFILE --testOutputFile TESTOUTPUTFILE --testGoldFile TESTGOLDFILE")
            print("******************************************************************************************************************************")
            print("Test file path is missing!")
            exit(1)

        if not args.testOutputFile:
            print("******************************************************************************************************************************")
            print("Usage main.py predictAndEvaluate lang [--html] --testFile TESTFILE --testOutputFile TESTOUTPUTFILE --testGoldFile TESTGOLDFILE")
            print("******************************************************************************************************************************")
            print("Output file path is missing!")
            exit(1)

        if not args.testGoldFile:
            print("******************************************************************************************************************************")
            print("Usage main.py predictAndEvaluate lang [--html] --testFile TESTFILE --testOutputFile TESTOUTPUTFILE --testGoldFile TESTGOLDFILE")
            print("******************************************************************************************************************************")
            print("Gold standard file path is missing!")
            exit(1)

        predictAndEvaluate(config, args.lang, args.testFile, args.testOutputFile, args.testGoldFile, args.html)

    # Train, test and evaluate
    elif args.mode == "sbdPipeline":
        sbdPipeline(config, args.lang)

    # Evaluation
    elif args.mode == "evaluate":

        if not args.testOutputFile:
            print("***************************************************************************************")
            print("Usage main.py evaluate lang --testOutputFile TESTOUTPUTFILE --testGoldFile TESTGOLDFILE")
            print("***************************************************************************************")
            print("The path of file to evaluate is missing!")
            exit(1)

        if not args.testGoldFile:
            print("***************************************************************************************")
            print("Usage main.py evaluate lang --testOutputFile TESTOUTPUTFILE --testGoldFile TESTGOLDFILE")
            print("***************************************************************************************")
            print("Gold standard file path is missing!")
            exit(1)

        evaluate(args.testOutputFile, args.testGoldFile)

if __name__ == '__main__':
    parseArgs()
