import re
import keras
import numpy as np
from keras.models import load_model
from preprocess import Preprocessing
import pandas as pd
import logging

class eosTest(object):

    def __init__(self):
        self.preproc = Preprocessing()

    def predict(self, testFile,
            modelFilename,
            indexFilename,
            testOutputFile,
            htmlOption,
            windowNgram,
            batchSize):
        """ Makes predictions on a testFile and outputs a file
        with the predicted sentences.
        # Arguments:
          testFile: the file to make predictions on
          modelFilename: the trained model file
          indexFilename: the char index file
          testOutputFile: file with the predicted sentences
          htmlOption: Specifies if the testFile is html or not
          windowNgram: the context window size to consider around a potential eos (in terms of a char number)
          batchSize: Size of batch of the model
        # Returns:
          outputSentences: list of the predicted sentences
        """

        # List of predicted sentences
        outputSentences = []

        # Loading the model
        model = load_model(modelFilename)

        # Reading the testFile
        with open(testFile, mode='r', encoding='utf-8') as f:
            testCorpus = f.read()

        # Get the clean text form a html file
        if htmlOption:
            textContent, soup = self.preproc.getCleanTextFromHtml(testCorpus)
            testCorpus = textContent

        # Building the potential eos ngrams from the input corpus
        charIndex = self.preproc.loadCharIndex(indexFilename)
        potentialEosNgrams = self.preproc.buildPotentialEosNgramsFromInput(testCorpus, windowNgram)

        # Make predictions and printing the resulting sentences
        beg = 0
        for pEos in potentialEosNgrams:
            start, ngram = pEos
            # Initializing the output vector
            dataSet = self.preproc.buildDataset([(-1.0, ngram)],charIndex,windowNgram)
            if len(dataSet) > 0:
                label, vector = dataSet[0]
                # Predict the ngram label
                predicted = model.predict(vector.reshape(1,2 * windowNgram + 1),batch_size=batchSize,verbose=0)
                if predicted[0][0] >= 0.5:
                    # Add a sentence if the prediction label >= 0.5
                    outputSentences.append(testCorpus[beg:start + 1].strip())
                    beg = start + 1

        # Output a file with predicted sentences
        with open(testOutputFile, mode='w', encoding='utf-8') as f:
            f.write("\n".join(outputSentences))

        # Output a html file tagged with sentences
        if htmlOption:
            outputSentences = [sents for cell in outputSentences for sents in cell.split('\n')]
            taggedHtmlContent = self.preproc.tagHtmlWithSentences(soup, outputSentences)
            with open(testFile+".tagged", mode='w', encoding='utf-8') as f:
                f.write(str(taggedHtmlContent))

        return outputSentences

    def evaluate(self, outputFile, testGoldFile):
        """ Compares the output File of a prediction and the goldStandard File
        and computes Precision, Recall and F-score
        # Arguments:
          testGoldFile: the gold standard file
          outputFile: the prediction outputFile
        # Returns:
          Precision, Recall and F-Score
        """

        # Building a dataframe of goldStandard sentences
        with open(testGoldFile, mode='r', encoding='utf-8') as f:
            goldSentences = f.read().splitlines()

        goldDf = pd.DataFrame(goldSentences)

        # Building a dataframe of output sentences
        with open(outputFile, mode='r', encoding='utf-8') as f:
            outSentences = f.read().splitlines()

        outDf = pd.DataFrame(outSentences)

        # Computing True positives, False positives and False Negatives
        tp = outDf.merge(goldDf, how = 'inner' ,indicator=False).shape[0]
        print ("tp = " + str(tp))
        fp = (outDf.merge(goldDf, how = 'outer' ,indicator=True).loc[lambda x : x['_merge']=='left_only']).shape[0]
        print ("fp = " + str(fp))
        fn = (outDf.merge(goldDf, how = 'outer' ,indicator=True).loc[lambda x : x['_merge']=='right_only']).shape[0]
        print ("fn = " + str(fn))

        # Computing precision, recall and f-score
        f_score = 0.0
        precision = float(tp / (tp + fp))
        recall = float(tp / (tp + fn))
        if (precision + recall) > 0:
            f_score = float((2 * precision * recall) / (precision + recall))

        print("Precision= " + str(precision) + "\t" + "Recall= " + str(recall) + "\t" + "F-Score= " + str(f_score))
        return precision, recall, f_score
