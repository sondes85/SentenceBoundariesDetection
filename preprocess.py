import re
import numpy as np
import joblib
import pandas as pd
from sklearn.model_selection import train_test_split
from bs4 import BeautifulSoup

class Preprocessing(object):

    def __init__(self):
        super()

    def buildNgramsfromTrainingCorpus(self, corpus, windowNgram):
        """ Builds a list of char ngrams from a training corpus with their labels.
        # Arguments
          corpus : Input raw text one sentence per line
          ngramWindow: the context window size to consider around a potential eos (in terms of a char number)
        # Returns
          A list of char ngrams with their labels (0.0 or 1.0)
        """

        # Positive examples pool
        eosNgrams = []
        # Negative examples pool
        nonEosNgrams = []

        eosRegex = '[\.:?!;][^\n]?[\n]'
        nonEosRegex = '[\.:?!;][^\s]?[ ]+'

        # Building positive examples pool
        for m in re.finditer(eosRegex, corpus):
            # Char sequence of 2*windowNgram around the eos
            eosNgram = corpus[m.start() - windowNgram:m.start()].replace("\n", " ") + corpus[m.start():m.start() + windowNgram + 1].replace("\n", " ")
            # eos has 1.0 as label
            eosNgramTuple = (1.0, eosNgram)
            eosNgrams.append(eosNgramTuple)

        # Building negative examples pool
        for m in re.finditer(nonEosRegex, corpus):
            # Char sequence of 2*windowNgram around the nonEos
            nonEosNgram = corpus[m.start() - windowNgram:m.start()].replace("\n", " ") + corpus[m.start():m.start() + windowNgram + 1].replace("\n", " ")
            # nonEos has 0.0 as label
            nonEosNgramTuple = (0.0, nonEosNgram)
            nonEosNgrams.append(nonEosNgramTuple)

        # List of all examples with their labels
        return eosNgrams + nonEosNgrams

    def buildPotentialEosNgramsFromInput(self, corpus, windowNgram):
        """ Builds a list of potential eos char ngrams from an input corpus
        with their starting positions as a label (to be replaced later with the real label).
        # Arguments
          corpus : Input raw text
          windowNgram: the context window size to consider around a potential eos (in terms of a char number)
        # Returns
          list of potential eos char ngrams with their starting positions as a label
        """

        punctRegex = '[\(\)`“”\"›〈⟨〈<‹»«‘’–\'``''\u0093\u0094]*'
        potentialEosStartPositions = []

        # Building the list of starting positions of potential eos
        for m in re.finditer(r'([\.:?!;])(\s+' + punctRegex + '|' + punctRegex + '\s+ | [\s\n]+)', corpus):
            potentialEosStartPositions.append(m.start())

        potentialEosNgrams = []
        # Extraction of a char sequence of 2*windowNgram (after cleaning multiple whitespaces) around the potential eos
        for pos in potentialEosStartPositions:
            leftContext = corpus[pos - (2 * windowNgram):pos]
            leftContext = re.sub('\s+', ' ', leftContext)
            rightContext = corpus[pos:pos + (2 * windowNgram)]
            rightContext = re.sub('\s+', ' ', rightContext)
            # The ngram contains also the potential eos
            potentialEosNgrams.append((pos,leftContext[-windowNgram:] + corpus[pos] + rightContext[1:windowNgram + 1]))

        # List of all potential eos ngrams with their starting positions
        return potentialEosNgrams

    def buildCharIndex(self, ngrams):
        """ Builds a dictionary of char indexes (useful for converting
        char ngrams to vectors of integers).
        # Arguments
            ngrams: The input set of char ngrams
        # Returns
            A dictionary of char indexes (index 0 is for unknown chars)
        """

        charIndex = {}
        id = 1

        for label, ngram in ngrams:
            for char in ngram:
                if char not in charIndex:
                    charIndex[char] = id
                    id += 1

        return charIndex

    def buildDataset(self, ngrams, charIndex, windowNgram):
        """ Converts the list of ngrams to numpy vectors of integers based on the char index.
        # Arguments
            ngrams: The input char ngrams from the training corpus
            charIndex: The dictionary of char indexes
            windowNgram: the context window size to consider around a potential eos (in terms of a char number)
        # Returns
            A dataset containing numpy vectors of converted ngrams
        """

        dataSet = []

        # Converting char ngrams to vectors of ids
        for label, ngram in ngrams:
            if len(ngram) == 2*windowNgram +1:
                ngramIds = []
                for char in ngram:
                    if char in charIndex:
                        ngramIds.append(charIndex[char])
                    else:
                        ngramIds.append(0) # unknown chars

                ngramVector = np.array([float(ngramIds[i]) for i in range(len(ngramIds))], dtype=float)
                dataSet.append((float(label),ngramVector))

        return dataSet

    def prepareData(self, trainingCorpus, trainRatioSize):
        """ Splits the input corpus into train, validation and test data.
        # Arguments
            trainingCorpus: The complete training corpus
        # Returns
            train, validation, and test data
        """

        # Reading input corpus
        with open(trainingCorpus, mode='r', encoding='utf-8') as f:
            sentences = f.read().splitlines()

        # Put sentences into a dataframe
        df = pd.DataFrame(sentences)

        # Split the data into train data (80%) and test data (20%)
        trainData, testData = train_test_split(df, test_size=1-trainRatioSize)

        # Building training corpus (part of the original)
        np.savetxt(trainingCorpus+".train", trainData.values, fmt='%s')

        # Building test goldStandard corpus (part of the original)
        np.savetxt(trainingCorpus+".test.gold", testData.values, fmt='%s')

        # Building test corpus (part of the original but without the \n separator)
        np.savetxt(trainingCorpus+".test", testData.values, fmt='%s', newline=" ")

    def getCleanTextFromHtml(self, htmlContent):
        """ Gets clean text from a html content.
        # Arguments:
          htmlContent: input html content
        # Returns:
          textContent: clean text content
        """
        soup = BeautifulSoup(htmlContent, 'html.parser')
        textContent = soup.body.text

        return textContent,soup

    def tagHtmlWithSentences(self, soup, outputSentences):
        """ Tag sentences in the html content (with <span> tags).
        # Arguments:
          htmlContent: input html content
          soup: the html content parser
          outputSentences: the predicted sentences
        # Returns:
          taggedHtmlContent: html content tagged with the predicted sentences
        """
        for s in outputSentences:
            if s is not None:
                sent = soup.body.find(text=re.compile("(?!<span>)"+s+"(?!<\/span>)"))
                if sent is not None :
                    new_span = soup.new_tag('span')
                    new_span.string = s
                    sent.replace_with(new_span)
        return soup

    def saveCharIndex(self, charIndex, indexFileName):
        """ Saves char index to a file
        # Arguments
            charIndex: The dictionary of char indexes
            indexFileName: The output filename
        """

        with open(indexFileName, 'wb') as f:
            joblib.dump(charIndex, f)

    def loadCharIndex(self,indexFileName):
        """ Loads char index from a file
        # Arguments
            indexFileName: The index filename
        # Returns
            char index
        """
        with open(indexFileName, 'rb') as f:
            return joblib.load(f)
