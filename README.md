# Sentence Boundary Detection with BiLSTM

Sentence Boundary Detection (SBD) consists in identifying sentences in a text or segmenting a text into sentences. This is useful for many tasks
in Natural Language Processiong like part-of-speech tagging, dependency parsing, Named Entity Recognition, machine translation, ... However, SBD is
non trivial especially because of the ambiguity of the period mark (.) that may be present in abbreviations, acronyms, mathematical numbers, ...

## Related work

Latest research in SBD seem to be focusing in machine learning techniques such as Hidden Markov Models (HMMs), Conditional Random Fields (CRFs), decision
trees, maximum entropy and Neural Networks (NNs). *Schweter and Ahmed (2019)* trained three different deep architectures of Neural Networks 
(LSTM, BiLSTM and CNN) and showed that these architectures outperform popular NLP frameworks like OpenNLP. BiLSTM is the architecture that gave the best results,
that's why I decided to implement this approach based on the paper:
([Deep-EOS: General-Purpose Neural Networks for Sentence Boundary Detection](https://www.semanticscholar.org/paper/Deep-EOS%3A-General-Purpose-Neural-Networks-for-Schweter-Ahmed/595796d854a738da1285582360509d6d1fa25cad)).

## BiLSTM model

The BiLSTM (Bidirectional Long Short-Term Memory) model used by *Schweter and Ahmed (2019)* for SBD uses a char-based context window instead of a word-based window to disambiguate a punctuation mark, which
makes the approach more robust to multi-lingual settings as it doesn't need any prior tokenization of the input text nor any additional resources.
The model disambiguates potential end of sentences markers followed by a whitespace or a line break given a context window of k characters including the punctuation
mark itself and the whitespace or the line break.
The model's parameters are the following:
*  Embedding size: 128
*  Hidden states: 256
*  Dropout: 0.2
*  Activation function: sigmoid

Some other hyperparameters are used:
*  Context window size: 5
*  Regularization strategy: Averaged stochastic gradient descent with a learning rate of 0.001
*  Batch Size: 32
*  Optimization strategy: Adam
*  Loss function: Binary cross-entropy

The model is evaluated using the precision, recall and F-score measures.

## User guide

I implemented the model using the Python 3.7.4 library. 
A configuration file (config.yaml) is used to store all the model parameters. Other parameters like the mode (train, predict, predictAndEvaluate, evaluate, 
sbdPipeline), the language (EN, FR for now) and input and output files are given on the command line.

### Training the model

The number of epochs in the training process is fixed to 5.

To train the model, the following command line should be used:

`python3 main.py train <language>`

The language must be **EN** (english) or **FR** (french) for now. The program uses a training file whose name is stored on the config parameter ***trainingFile***.
This parameter should be set before running the training script as well as all the training parameters. The program also saves the trained model into a file 
whose name is stored on the config parameter ***modelFilename*** and the characters index (vocab) into a file whose name is stored on the config 
parameter ***indexFilename***.

### Making predictions

To make predictions on a test file and save them into an output file, the following command line is used:

`python3 main.py predict <language> --testFile <testFilePath> --testOutputFile <testOutputFilePath> --html(optional)`

The program makes predictions on a test file (***testFilePath***) using a model file whose name is stored on the config parameter ***modeFilename*** and an 
index File whose name is stored on the config parameter ***indexFilename***. It also outputs a file of predicted sentences (***testOutputFilePath***).

The --html option is put only if the test file is a html file. In this case, the program outputs an additional file containing the html content with sentences
marked by `<span>` tags and whose path is the same as the (***testFilePath***) with an additional extension (.tagged).

### Making predictions and evaluating them

To make predictions on a test file and evaluate them with respect to a gold standard file, the following command line is used:

`python3 main.py predictAndEvaluate <language> --testFile <testFilePath> --testOutputFile <testOutputFilePath> --testGoldFile <testGoldFilePath> --html(optional)`

Other than making predictions, this program evaluates the predicted sentences (***testOutputFilePath***) with respect to a gold standard 
file (***testGoldFilePath***) containing the sentences that should be identified (one sentence per line) and prints Precision, Recall and F-score measures on the
screen.

The --html option is put only if the test file is a html file. In this case, the program outputs an additional file containing the html content with sentences
marked by `<span>` tags and whose path is the same as the (***testFilePath***) with an additional extension (.tagged).

### Evaluating predictions

To evaluate a test output file with respect to a gold standard file, the following command line is used:

`python3 main.py evaluate <language> --testOutputFile <testOutputFilePath> --testGoldFile <testGoldFilePath>`

Predicted sentences (***testOutputFilePath***) are compared with sentences from the gold standard file (***testGoldFilePath***). Evaluation metrics are then
printed on the screen.

### Train, predict and evaluate pipeline

Given a training corpus, we can automatically split it into train and test sets and process the whole pipeline of training, predicting and evaluating. 
To do so, the following command line is used:

`python3 main.py sbdPipeline <language>`

The program needs a single file whose name is stored on the config parameter ***trainingFile***. Given the training file path, the program creates four files 
adding four extensions to the original path (**.train** for the training file (80% of the original training file)), (**.test.gold** for the gold standard file (20% of 
the original training file)), (**.test** for the test file which is te same as the gold standard file but without the line breaks) and finally (**.test.output**) for 
the output file that will contain the predicted sentences (one per line). Evaluation metrics are then printed on the screen. The pipeline also saves the trained
model and the vocab into files whose names are in the config parameters ***modelFilename*** and ***indexFilename***.

## Evaluation of the model

The evaluation process I implemented is different from the one made by the paper authors and more strict: I consider a sentence as a right prediction only if its 
start and end positions are right. It means that, for a sentence which has been splitted wrongly into two pieces, I have no right predictions even if the second 
end of sentence has been correctly predicted.

The evaluation measures used are: Precision, Recall and F-Score.

### Task1: The Europarl parallel corpus

I evaluated the model on two corpora from [The Europarl parallel corpus](https://www.statmt.org/europarl/): ***europarl-v7.fr-en.en*** for english and 
***europarl-v7.fr-en.fr*** for french. These corpora (one sentence per line) were splitted into train data (80%) and test data (20%) (I run the commands: 
`python3 main.py sbdPipeline EN` and `python3 main.py sbdPipeline FR` after setting the config parameter ***trainingFile*** for both english and french).

The evaluation results are shown below:

| **Language** | **Train Data Size** | **Test Data Size** | **Precision** | **Recall** | **F-Score** |
| ------ | ------ | ------ | ------ | ------ | ------ |
| **English** | 1,749,118 | 437,052 | 95.13 | 96.75 | 95.93 |
| **French** | 1,818,146 | 454,535 | 92.32 | 95.35 | 93.81 |

The ***europarl-v7.fr-en.en*** and ***europarl-v7.fr-en.fr*** are also available respectively on the folders data/EN and data/FR of the project. I also saved
tho model trained on these corpora into the same folders (best_model_wholeCorpus.hdf5 and charIndex_wholeCorpus.pkl in each folder). They may be used directly to
make predictions on new test files.

### Task2: The SETimes parallel corpus

I trained the model on the ***europarl-v7.fr-en.en*** whole corpus and test it on the ***setimes.el-en.en.txt*** english corpus from 
[The SETimes parallel corpus](http://nlp.ffzg.hr/resources/corpora/setimes/).

The evaluation results are shown below:

| **Language** | **Data Size** | **Precision** | **Recall** | **F-Score** |
| ------ | ------ | ------ | ------ | ------ |
| **English** | 181,600 | 74.64 | 71.51 | 73.04 |

The F-Score rate is lower than the one obtained when testing on the ***europarl-v7.fr-en.en*** essentially because colon is never considered as an end of 
sentence in ***setimes.el-en.en.txt*** corpus. Let's take an example:

The portion of text "***Previous elections, Minxhozi said, have set a bad precedent: each time, after the process is organised, political parties rush to criticise 
it and make changes to the election code.***" is considered as a sentence in the corpus while our model splits it into two sentences: 
"***Previous elections, Minxhozi said, have set a bad precedent:***" and "***each time, after the process is organised, political parties rush to criticise 
it and make changes to the election code.***", which is not wrong.

### Comparison with Spacy

Spacy sentence segmenter is based on a statistical model that uses the dependency parse to determine sentence boundaries, meaning that it is dependent on
the tokenization and dependency parsing components.
I tested Spacy as an out-of-the-box component on a sample (of size 7,000) from the ***europarl-v7.fr-en.en*** and compared the results with those of our model.

| **Model** | **Precision** | **Recall** | **F-Score** |
| ------ | ------ | ------ | ------ |
| **BiLSTM** | 80.80 | 86.97 | 83.77 |
| **Spacy** | 81.30 | 87.76 | 84.41 |

As shown in the table above, Spacy slightly outperforms BiLSTM but we can not draw a conclusion from this because they have not been trained on the same corpos.
However, it is good to know that there are some competitive NLP out-of-the-box tools.

Spacy designers mentioned that if texts are closer to general-purpose news or web text, this should work well out-of-the-box. However, for social media or 
conversational text that doesn’t follow the same rules, the application may benefit from a custom rule-based implementation. I made a test on a complicated 
conversational html text: ([Pride and prejudice Chapter 1](https://www.w3.org/Mobile/training/mobile_course/resources/pandp/chapter1_no_vp.html)). The results 
given by Spacy and our model are shown in the table below:

| **Model** | **Precision** | **Recall** | **F-Score** |
| ------ | ------ | ------ | ------ |
| **Spacy** | 04.91 | 14.28 | 07.31 |
| **BiLSTM** | 42.42 | 66.67 | 51.85 |

Results show that BiLSTM is more robust to conversational texts.



